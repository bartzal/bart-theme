<?php
/*
 *  Author: Bart Zalas
 */


define("THEME_DIR", get_template_directory_uri());
/*--- REMOVE GENERATOR META TAG ---*/
remove_action('wp_head', 'wp_generator');

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 400, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 300, 300, array( 'center', 'center' )); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

}

// Crop post image thumbnail
//set_post_thumbnail_size( 300, 300, true ); // 50 pixels wide by 50 pixels tall, crop mode
//set_post_thumbnail_size( 300, 300, array( 'center', 'center')  );

// Read more
// Replaces the excerpt "Read More" text by a link
function new_excerpt_more($more) {
       global $post;
	return '<a class="moretag" href="'. get_permalink($post->ID) . '">[read more]</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');


/// Navigation - menu
 // this is the setup function.
function topfoot_barttheme_setup() {

    register_nav_menus(
        array(
            'footer_nav' => __( 'Footer Menu', 'barttheme' ), // footer menu
            'top_menu' => __( 'Top Menu', 'barttheme' ), // top_menu location
        )
    );

}
// this will hook the setup function in after other setup actions.
add_action( 'after_setup_theme', 'topfoot_barttheme_setup' );

// Register Custom Navigation Walker
require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';


// Sidebars & Widgets Areas

function bart_widgets_init() {

    register_sidebar(array(
    	'id' => 'sidebar1',
    	'name' => 'Blog Sidebar',
    	'description' => 'Used only on the blog page template.',
    	'before_widget' => '<div id="%1$s" class="widget %2$s">',
    	'after_widget' => '</div>',
    	'before_title' => '<h4 class="widgettitle">',
    	'after_title' => '</h4>',
    ));

    register_sidebar(array(
      'id' => 'footer1',
      'name' => 'Footer 1',
      'before_widget' => '<div id="%1$s" class="widget span4 %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h4 class="widgettitle">',
      'after_title' => '</h4>',
    ));

    register_sidebar(array(
      'id' => 'footer2',
      'name' => 'Footer 2',
      'before_widget' => '<div id="%1$s" class="widget span4 %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h4 class="widgettitle">',
      'after_title' => '</h4>',
    ));

    register_sidebar(array(
      'id' => 'footer3',
      'name' => 'Footer 3',
      'before_widget' => '<div id="%1$s" class="widget span4 %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h4 class="widgettitle">',
      'after_title' => '</h4>',
    ));
}
add_action( 'widgets_init', 'bart_widgets_init' );


/*
* Creating a function to create our CPT
*/
 
function custom_post_type() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Events', 'Post Type General Name', 'barttheme' ),
        'singular_name'       => _x( 'Event', 'Post Type Singular Name', 'barttheme' ),
        'menu_name'           => __( 'Events', 'barttheme' ),
        'parent_item_colon'   => __( 'Parent Event', 'barttheme' ),
        'all_items'           => __( 'All Events', 'barttheme' ),
        'view_item'           => __( 'View Event', 'barttheme' ),
        'add_new_item'        => __( 'Add New Event', 'barttheme' ),
        'add_new'             => __( 'Add New', 'barttheme' ),
        'edit_item'           => __( 'Edit Event', 'barttheme' ),
        'update_item'         => __( 'Update Event', 'bartheme' ),
        'search_items'        => __( 'Search Event', 'barttheme' ),
        'not_found'           => __( 'Not Found', 'barttheme' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'barttheme' ),
    );

// Set other options for Custom Post Type
    $args = array(
        'label'               => __( 'Events', 'barttheme' ),
        'description'         => __( 'Events around', 'barttheme' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        'taxonomies'          => array( 'category' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );

    // Registering your Custom Post Type
    register_post_type( 'event', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action( 'init', 'custom_post_type', 0 );


// enqueue styles
if( !function_exists("theme_styles") ) {
    function theme_styles() {
        // This is the compiled css file from LESS - with bootstrap,modernizr and responsive css
        wp_register_style( 'styles', get_template_directory_uri() . '/style.css', array(),'1.0', 'all' );

        wp_enqueue_style( 'styles' );
    }
}
add_action( 'wp_enqueue_scripts', 'theme_styles' );

// enqueue javascript
if( !function_exists( "theme_js" ) ) {
  function theme_js(){

    wp_register_script( 'scripts',
      get_template_directory_uri() . '/js/scripts.js',
      array('jquery'),
      '1.2' );

    wp_enqueue_script('scripts');

  }
}
add_action( 'wp_enqueue_scripts', 'theme_js' );



?>
