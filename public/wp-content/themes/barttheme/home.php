<?php get_header(); ?>

    <div class="content">
	<div class="row">
      <?php get_sidebar(); // sidebar 1 ?>


                <div class="container col-md-9" id="main">

                <div class="row">
			<div class="col-md-12">

                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                            <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="blog-page-content">
				<div class="row post-home-row">
				<div class="col-lg-8 col-md-8 post-home">
                                        <h2><a href="<?php the_permalink(); ?>" class="category-title-link"><?php the_title(); ?></a></h2>

                                        <div class="category-post">


                                           <?php the_excerpt(); ?>
                                           <h6 class="article-meta-extra">


                                                <?php the_date(get_option('date_format')); ?> &#62; <?php the_author_posts_link(); ?> &#62;
                                                <?php if (has_category() && !has_category('Uncategorized')) : ?>

                                                        <?php the_category('  |  '); ?>

                                                    <?php else : ?>

                                                    <?php endif; ?>

                                            </h6>

                                        </div><!-- END CATEGORY POST -->

                </div> <!--end post-home-->
                <div class="col-lg-4 col-md-4 thumbnail-class">
                                    <?php if (has_post_thumbnail()) : ?>

                                        <figure class="article-preview-image">

                                            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('custom-size', array('class' => 'circle-thumb-image')); ?></a>

                                        </figure>

                                    <?php else : ?>

                                    <?php endif; ?>
				</div> <!-- /end thumbnail-class-->
                </div> <!-- /row -->
                </div> <!--/blog-page-content-->
                </div> <!-- /row-->
                <div class="seperate-posts"><hr></div>
                        <?php endwhile; ?>

                        <?php else : ?>

                        <article class="no-posts">

                            <h1>No posts were found.</h1>

                        </article>

                        <?php endif; ?>

                    </div>

                </div>

                <div class="clearfix"></div>

                <div class="article-nav clearfix">

                    <p class="article-nav-next pull-right"><?php previous_posts_link(__('Newer Posts »')); ?></p>

                    <p class="article-nav-prev pull-left"><?php next_posts_link(__('« Older Posts')); ?></p>

                </div> <!-- article-nav -->
                </div> <!-- /row -->
                </div> <!-- /container -->

                <div class="clearfix"></div>


                <div class="clearfix"></div>

	</div> <!--/row-->
    </div><!-- END content -->

<?php get_footer(); ?>
