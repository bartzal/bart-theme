<div id="sidebar1" class="fluid-sidebar col-md-2 col-xs-12 sidebar span4" role="complementary">


  <!-- nav -->
  <nav class="navbar navbar-expand-lg navbar-light">

  <button id="btnClick2" class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">

   <span id="navbar-hamburger" class="navbar-toggler-icon2">MENU</span>
   <span id="navbar-close" class="glyphicon glyphicon-remove" style="display:none;"></span>
  </button>

    <?php
                                wp_nav_menu( array(
                                        'menu'              => 'top_menu',
                                        'depth'             => 1,
                                        'container'         => 'div',
                                        'container_class'   => 'collapse navbar-collapse',
                                        'container_id'      => 'navbarSupportedContent',
                                        'menu_class'        => 'navbar-nav mr-auto flex-column',
                                        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                        'walker'            => new wp_bootstrap_navwalker())
                                );
                        ?>
  </nav>
  <!-- /end nav -->

<script>
jQuery('#btnClick2').on('click',function(){
    if($('#navbar-hamburger').css('display')!='none'){
    $('#navbar-close').html('X').show().siblings('span').hide();
    }else if($('#navbar-close').css('display')!='none'){
        $('#navbar-hamburger').show().siblings('span').hide();
    }
});

</script>

</div>
