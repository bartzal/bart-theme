<?php get_header(); ?>

    <div class="content">
	<div class="row">
      <?php get_sidebar(); // sidebar 1 ?>


                <div class="container col-md-9" id="main">

                <div class="row">
			<div class="col-md-12">
	
  <div class="gridBox">
		<div class="boxes-container">
			<?php
				$args = array(
					'post_type' 		=> 'event', 		// replace "post" if you want to display different post-type
					'posts_per_page'	=> -1			//  show all posts
				);

				// The Query
				$the_query = new WP_Query( $args );

				// The Loop
				if ( $the_query->have_posts() ) {

					$c = 1; 	// counter
					$bpr = 2; 	// number of column in each row

					while ( $the_query->have_posts() ) : $the_query->the_post();
						?>
							<div class="grid-boxes col-md-6 <?php echo (($c != $bpr) ? 'margin_right' : ''); ?>">
								
								<!-- Thumbnail commented -->
								<?php /* <div class="grid-thumbnail">
									<?php if ( has_post_thumbnail()) { ?>
										<div class="alignleft">
											<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
	

											
											
											<?php the_post_thumbnail(); ?>
											</a>
										</div>
									<?php } else { ?>
										<img src="<?php bloginfo('template_directory'); ?>/images/no-thumbnail.jpg" alt="No Thumbnail" />
									<?php } ?>
								</div> */?>
								<p>
																		
											<div class="category-post"> <h6 class="article-meta-extra"> <?php the_date(get_option('date_format')); ?> &#62; <?php the_author_posts_link(); ?> &#62; <?php if (has_category() && !has_category('Uncategorized')) : ?> <?php the_category(' | '); ?> <?php else : ?> <?php endif; ?> </h6> </div><!-- END CATEGORY POST --> 
								</p>
								<h2><a href="<?php the_permalink(); ?>" class="category-title-link"><?php the_title(); ?></a></h2> 
								<p>  <?php the_excerpt(); ?></p>
							</div>
						<?php

						// we add a condition here to see if counter is equal to the number of column.
						if( $c == $bpr ) {
							echo '<div class="clear"></div>';
							$c = 0; 	// back the counter to 0 if the condition above is true.
						}
						$c++; 			// increment counter of 1 until it hits the limit of column of every row.

					endwhile;
				} else {

					// no posts found
					_e( '<h2>Oops!</h2>', 'rys' );
					_e( '<p>Sorry, seems there are no post at the moment.</p>', 'rys' );

				}

				/* Restore original Post Data */
				wp_reset_postdata();
			?>
		</div>
		<div class="clear"></div>
	</div>
	</div>
	

			
                       
                <div class="clearfix"></div>


                <div class="clearfix"></div>
    </div></div></div>
	</div> <!--/row-->
    </div><!-- END content -->

<?php get_footer(); ?>
