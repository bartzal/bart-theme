
			<!-- footer -->
			<footer class="footer" id="myFooter" role="contentinfo">

			 <!-- footer widgets -->
			<div id="inner-footer" class="clearfix">

		          <div id="widget-footer" class="clearfix row-fluid">
		            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer1') ) : ?>
		            <?php endif; ?>
		            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer2') ) : ?>
		            <?php endif; ?>
		            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer3') ) : ?>
		            <?php endif; ?>
		          </div>
			</div>

				<!-- copyright -->
				<p class="copyright">
					&copy; <?php echo date('Y'); ?> by Alan Schweitzer
					<div class="follow-my">Follow my adventures
						<a href="#" class="fab fa-instagram"></a>
						<a href="#" class="fab fa-twitter"></a>
						<a href="#" class="fab fa-vimeo"></a>
						<a href="#" class="fab fa-youtube"></a>
					</div>

					<div class="imprint">
						<a href="https://imprint.de/" title="Imprint">Imprint</a>.
					</div>
				</p>
				<!-- /copyright -->

			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>

	</body>
</html>

