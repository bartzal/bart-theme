<?php get_header(); ?>

			<div class="content">
				<div class="row">
                                <?php get_sidebar(); // sidebar 1 ?>

				<div id="main" class="container col-md-8" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

						<header>


							<div class="page-header"><h1 class="single-title" itemprop="headline"><?php the_title(); ?></h1></div>


						</header> <!-- end article header -->

						<section class="post_content clearfix" itemprop="articleBody">
							<?php the_content(); ?>

							<?php wp_link_pages(); ?>

						</section> <!-- end article section -->
                        <?php the_post_thumbnail( 'large' ); ?>
	
                        <footer>
                            <div class="category-post">


                            <h6 class="article-meta-extra">


                                                <?php the_date(get_option('date_format')); ?> &#62; <?php the_author_posts_link(); ?> &#62;
                                                <?php if (has_category() && !has_category('Uncategorized')) : ?>

                                                        <?php the_category('  |  '); ?>

                                                    <?php else : ?>

                                                    <?php endif; ?>

                            </h6>

                            </div><!-- END CATEGORY POST -->
                                    

							<?php the_tags('<p class="tags"><span class="tags-title">' . __("Tags","barttheme") . ':</span> ', ' ', '</p>'); ?>

							<?php
							// only show edit button if user has permission to edit posts
							if( $user_level > 0 ) {
							?>
							<a href="<?php echo get_edit_post_link(); ?>" class="btn btn-success edit-post"><i class="icon-pencil icon-white"></i> <?php _e("Edit post","barttheme"); ?></a>
							<?php } ?>

						</footer> <!-- end article footer -->

					</article> <!-- end article -->

					<?php comments_template('',true); ?>

					<?php endwhile; ?>

					<?php else : ?>

					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "barttheme"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "barttheme"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>

					<?php endif; ?>

				</div> <!-- end #main -->

				</div>
			</div> <!-- end #content -->

<?php get_footer(); ?>
