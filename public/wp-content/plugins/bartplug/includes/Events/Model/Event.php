<?php declare(strict_types=1); # -*- coding: utf-8 -*-

namespace bartplug\includes\Events\Model;

require plugin_dir_path( __FILE__ ) . 'ContactPerson.php';
require plugin_dir_path( __FILE__ ) . 'Location.php';

class Event
{
    public static function fromPost(\WP_Post $post): Event
    {
        return new static($post);
    }

    public function id(): int
    {
        return 0;
    }

    public function startDate(): \DateTimeImmutable
    {
        return new \DateTimeImmutable('now');
    }

    public function endDate(): \DateTimeImmutable
    {
        return new \DateTimeImmutable('now');
    }

    public function registrationEnd(): \DateTimeImmutable
    {
        return new \DateTimeImmutable('now');
    }

    public function includedInPrice(): array
    {
        return [
            'drinks',
            'food',
        ];
    }

    public function subscribedMin(): int
    {
        return 1;
    }

    public function subscribedMax(): int
    {
        return 5;
    }


    public function additionalNotes(): string
    {
        return 'Additional information about this event can be found on tour website www.example.com';
    }

    public function location(): Location
    {
        return new Location();

    }

    public function contactPerson(): ContactPerson
    {
        return new ContactPerson();
    }
    public function __construct(){
        add_shortcode( 'id_event', array($this , 'id' ));
        add_shortcode( 'start_date', array($this , 'startDate' )); //not work
        add_shortcode( 'end_date', array($this , 'endDate' )); //not work
        add_shortcode( 'registration_end', array($this , 'registrationEnd' )); //not work
	add_shortcode( 'subscribtion_applay', array($this , 'subscribedMin' ));
        add_shortcode( 'subscribtion_applay_max', array($this , 'subscribedMax' ));
        add_shortcode( 'additional_notes', array($this , 'additionalNotes' ));
	add_shortcode( 'all_details', array($this , 'additionalNotes' , 'id' ));


    }


}

new Event();


?>
