<?php declare(strict_types=1); # -*- coding: utf-8 -*-

namespace bartplug\includes\Events\Model;



final class ContactPerson
{

    public function firstName(): string
    {
        return 'Max';
    }

    public function lastName(): string
    {
        return 'Mustermann';
    }

    public function email(): string
    {
        return 'max@mustermann.com';
    }

    public function position(): string
    {
        return 'Chief of Organization';
    }

    public function telephone(): string
    {
        return '+49 1234 567 89';

    }

    public function __construct(){
        add_shortcode( 'contactperson1', array($this , 'firstName'));
        add_shortcode( 'contactperson2', array($this , 'lastName'));
        add_shortcode( 'contactperson3', array($this , 'email'));
        add_shortcode( 'contactperson4', array($this , 'position'));
        add_shortcode( 'contactperson5', array($this , 'telephone'));



    }


}

new ContactPerson();


?>
