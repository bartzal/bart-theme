<?php

/**
 * Fired during plugin activation
 *
 * @link       bartplug.com/author
 * @since      1.0.0
 *
 * @package    Bartplug
 * @subpackage Bartplug/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Bartplug
 * @subpackage Bartplug/includes
 * @author     Bart <bart.zalas@gmail.com>
 */
class Bartplug_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
