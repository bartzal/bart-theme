<?php

/**
 * Fired during plugin deactivation
 *
 * @link       bartplug.com/author
 * @since      1.0.0
 *
 * @package    Bartplug
 * @subpackage Bartplug/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Bartplug
 * @subpackage Bartplug/includes
 * @author     Bart <bart.zalas@gmail.com>
 */
class Bartplug_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
